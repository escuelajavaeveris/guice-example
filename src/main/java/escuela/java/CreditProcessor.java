package escuela.java;

/**
 * Created by hgranjal on 19/06/2018.
 */
public interface CreditProcessor {
    boolean isValid(CreditCard creditCard);
}
