package escuela.java;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

/**
 * Created by hgranjal on 19/06/2018.
 */
public class AuditInterceptor implements MethodInterceptor {
    @Override
    public Object invoke(MethodInvocation methodInvocation) throws Throwable {
        System.out.println(
                "Class: " + methodInvocation.getMethod().getDeclaringClass().getSimpleName()
                + ", Method: " + methodInvocation.getMethod().getName());
        return methodInvocation.proceed();
    }
}
