package escuela.java;

/**
 * Created by hgranjal on 19/06/2018.
 */
public interface BookingService {
    BookingInfo bookingPlane(PlaneBookingOrder planeBookingOrder, CreditCard creditCard);
    CreditProcessor getCreditProcessor();
}
