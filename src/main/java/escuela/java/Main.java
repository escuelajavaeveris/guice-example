package escuela.java;

import com.google.inject.Guice;
import com.google.inject.Injector;


/**
 * Created by hgranjal on 19/06/2018.
 */
public class Main {

    public static void main(String[] args) {
        //without DI
//        CreditProcessor creditProcessor = new CreditProcessorDB();
//        BookingService bookingService = new BookingServiceImpl(creditProcessor);
//        bookingService.bookingPlane(null, null);

        //with DI (property)
//        Injector injector = Guice.createInjector(new BookingModule());
//        BookingService bookingService1 = injector.getInstance(BookingService.class);
//
//        System.out.println(bookingService1.getClass().toString());
//        System.out.println(bookingService1.getCreditProcessor().getClass().toString());

        //with DI (provides)
//        Injector injector = Guice.createInjector(new BookingModule());
//        Connection connection = injector.getInstance(Connection.class);
//
//        System.out.println(connection.getClass().toString());
//        connection.checkConnection();
//
        //with DI (implemented by)
//        Injector injector = Guice.createInjector(new BookingModule());
//        AuditSystem auditSystem = injector.getInstance(AuditSystem.class);
//        System.out.println(auditSystem.getClass().toString());

        //with DI (named annotation)
//        Injector injector = Guice.createInjector(new AuditModule());
//        AuditSystem auditSystem1 = injector.getInstance(AuditSystemALL.class);
//        AuditSystem auditSystem2 = injector.getInstance(AuditSystemALL.class);
//        //Singleton
//        System.out.println(auditSystem1.hashCode());
//        System.out.println(auditSystem2.hashCode());
//        System.out.println(auditSystem1.equals(auditSystem2));
//        System.out.println(auditSystem.getClass().toString());
//        auditSystem.audit("Hello");

        //with DI (Interceptors)
        Injector interceptorInjector = Guice.createInjector(new BookingModule(), new InterceptorModule());
        BookingService bookingServiceIntercepted =
                interceptorInjector.getInstance(BookingService.class);
        bookingServiceIntercepted.bookingPlane(new PlaneBookingOrder(), new CreditCard());



    }
}
