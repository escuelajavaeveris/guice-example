package escuela.java;

/**
 * Created by hgranjal on 19/06/2018.
 */
public class AuditSystemES implements AuditSystem {
    @Override
    public void audit(String data) {
        System.out.println("Audit ES -> " + data);
    }
}
