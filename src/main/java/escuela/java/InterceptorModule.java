package escuela.java;

import com.google.inject.AbstractModule;
import com.google.inject.matcher.Matchers;

import static com.google.inject.matcher.Matchers.inSubpackage;

/**
 * Created by hgranjal on 19/06/2018.
 */
public class InterceptorModule extends AbstractModule {
    @Override
    protected void configure() {
        bindInterceptor(
              inSubpackage("escuela.java"),
                Matchers.any(),
                new AuditInterceptor()
        );
    }
}
