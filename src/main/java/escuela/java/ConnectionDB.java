package escuela.java;

/**
 * Created by hgranjal on 19/06/2018.
 */
public class ConnectionDB implements Connection {

    private String driver;
    private String url;

    public ConnectionDB(String driver, String url) {
        this.driver = driver;
        this.url = url;
    }

    @Override
    public void checkConnection() {
        System.out.println("Connection data: ");
        System.out.println("driver: " + driver);
        System.out.println("url: " + url);
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
