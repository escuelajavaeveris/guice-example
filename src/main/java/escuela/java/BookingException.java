package escuela.java;

/**
 * Created by hgranjal on 19/06/2018.
 */
public class BookingException extends RuntimeException {
    public BookingException(String s) {
        super(s);
    }
}
