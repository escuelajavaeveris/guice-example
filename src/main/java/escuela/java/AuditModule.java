package escuela.java;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;

/**
 * Created by hgranjal on 19/06/2018.
 */
public class AuditModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(AuditSystem.class).annotatedWith(Names.named("Primary"))
                .to(AuditSystemES.class);
        bind(AuditSystem.class).annotatedWith(Names.named("Secondary"))
                .to(AuditSystemDB.class);
    }
}
