package escuela.java;

import com.google.inject.Inject;

/**
 * Created by hgranjal on 19/06/2018.
 */
public class BookingServiceImpl implements BookingService {

    private CreditProcessor creditProcessor;

    @Inject
    public BookingServiceImpl(CreditProcessor creditProcessor) {
        this.creditProcessor = creditProcessor;
    }

    @Override
    public BookingInfo bookingPlane(PlaneBookingOrder planeBookingOrder, CreditCard creditCard) {
        if (!creditProcessor.isValid(creditCard)) {
            throw new BookingException("Credit card is not valid.");
        }
        return doBookingPlane(planeBookingOrder);
    }

    public CreditProcessor getCreditProcessor() {
        return this.creditProcessor;
    }

    public void setCreditProcessor(CreditProcessor creditProcessor) {
        this.creditProcessor = creditProcessor;
    }

    private BookingInfo doBookingPlane(PlaneBookingOrder planeBookingOrder) {
        BookingInfo bookingInfo = new BookingInfo();
        bookingInfo.setBookingOrder(planeBookingOrder);
        bookingInfo.setBookingStatus(BookingStatus.PENDING);
        bookingInfo.setCheckinDone(false);
        return bookingInfo;
    }

}
