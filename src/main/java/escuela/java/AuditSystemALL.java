package escuela.java;

import com.google.inject.Inject;
import com.google.inject.name.Named;

import javax.inject.Singleton;

/**
 * Created by hgranjal on 19/06/2018.
 */
@Singleton
public class AuditSystemALL implements AuditSystem {

    private final AuditSystem auditSystemPrimary;
    private final AuditSystem auditSystemSecondary;

    @Inject
    public AuditSystemALL(@Named("Primary") AuditSystem auditSystemPrimary,
                          @Named("Secondary") AuditSystem auditSystemSecondary) {
        this.auditSystemPrimary = auditSystemPrimary;
        this.auditSystemSecondary = auditSystemSecondary;
    }

    @Override
    public void audit(String data) {
        auditSystemPrimary.audit(data);
        auditSystemSecondary.audit(data);
    }
}
