package escuela.java;

/**
 * Created by hgranjal on 19/06/2018.
 */
public interface Connection {
    void checkConnection();
    String getDriver();
    String getUrl();
}
