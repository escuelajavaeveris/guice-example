package escuela.java;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;

/**
 * Created by hgranjal on 19/06/2018.
 */
public class BookingModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(CreditProcessor.class).to(CreditProcessorDB.class);
        bind(BookingService.class).to(BookingServiceImpl.class);
    }

    @Provides
    Connection provideConnection() {
        Connection connectionDB = new ConnectionDB("driver", "url");
        return connectionDB;
    }

}
