package escuela.java;

import java.time.LocalDate;

/**
 * Created by hgranjal on 19/06/2018.
 */
public class CreditProcessorDB implements CreditProcessor {
    @Override
    public boolean isValid(CreditCard creditCard) {
        return creditCard != null
                && isValidNumber(creditCard.getNumber())
                && isValidCcv(creditCard.getNumber(), creditCard.getCcv())
                && isNotExpired(creditCard.getExpirationDate());
    }

    private boolean isValidNumber(Long number) {
        return number != null;
    }

    private boolean isValidCcv(Long number, Integer ccv) {
        return number != null && ccv != null;
    }

    private boolean isNotExpired(LocalDate expirationDate) {
        return expirationDate != null && expirationDate.isAfter(LocalDate.now());
    }
}
