package escuela.java;

import java.time.LocalDate;

/**
 * Created by hgranjal on 19/06/2018.
 */
public class CreditCard {
    private CreditCardType creditCardType;
    private String owner;
    private Long number;
    private Integer ccv;
    private LocalDate expirationDate;

    public CreditCard() {
    }

    public CreditCard(CreditCardType creditCardType, String owner, Long number, Integer ccv, LocalDate expirationDate) {
        this.creditCardType = creditCardType;
        this.owner = owner;
        this.number = number;
        this.ccv = ccv;
        this.expirationDate = expirationDate;

    }

    public CreditCardType getCreditCardType() {
        return creditCardType;
    }

    public void setCreditCardType(CreditCardType creditCardType) {
        this.creditCardType = creditCardType;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public Integer getCcv() {
        return ccv;
    }

    public void setCcv(Integer ccv) {
        this.ccv = ccv;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }
}
