package escuela.java;

import java.time.LocalDate;

/**
 * Created by hgranjal on 19/06/2018.
 */
public class PlaneBookingOrder {
    private Integer orderNumber;
    private String airline;
    private String departure;
    private String arrival;
    private LocalDate departureDate;
    private LocalDate arrivalDate;
    private String passengerName;

    public PlaneBookingOrder() {}

    public PlaneBookingOrder(Integer orderNumber, String airline, String departure, String arrival, LocalDate departureDate, LocalDate arrivalDate, String passengerName) {
        this.orderNumber = orderNumber;
        this.airline = airline;
        this.departure = departure;
        this.arrival = arrival;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
        this.passengerName = passengerName;
    }

    public Integer getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Integer orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    public LocalDate getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(LocalDate departureDate) {
        this.departureDate = departureDate;
    }

    public LocalDate getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(LocalDate arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getPassengerName() {
        return passengerName;
    }

    public void setPassengerName(String passengerName) {
        this.passengerName = passengerName;
    }
}
