package escuela.java;

/**
 * Created by hgranjal on 19/06/2018.
 */
public enum BookingStatus {
    CONFIRMED, PENDING, CANCELLED;
}
