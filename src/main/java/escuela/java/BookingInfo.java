package escuela.java;

/**
 * Created by hgranjal on 19/06/2018.
 */
public class BookingInfo {
    private PlaneBookingOrder bookingOrder;
    private BookingStatus bookingStatus;
    private Boolean checkinDone;

    public BookingInfo() {
    }

    public BookingInfo(PlaneBookingOrder bookingOrder, BookingStatus bookingStatus, Boolean checkinDone) {
        this.bookingOrder = bookingOrder;
        this.bookingStatus = bookingStatus;
        this.checkinDone = checkinDone;
    }

    public PlaneBookingOrder getBookingOrder() {
        return bookingOrder;
    }

    public void setBookingOrder(PlaneBookingOrder bookingOrder) {
        this.bookingOrder = bookingOrder;
    }

    public BookingStatus getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(BookingStatus bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public Boolean getCheckinDone() {
        return checkinDone;
    }

    public void setCheckinDone(Boolean checkinDone) {
        this.checkinDone = checkinDone;
    }
}
