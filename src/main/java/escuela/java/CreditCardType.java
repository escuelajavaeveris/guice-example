package escuela.java;

/**
 * Created by hgranjal on 19/06/2018.
 */
public enum CreditCardType {
    VISADEBIT("Visa Debit"),
    VISACREDIT("Visa Credit"),
    AMEX("American Express");

    private final String type;

    CreditCardType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public static CreditCardType findByType(String type) {
        for (CreditCardType creditCardType : CreditCardType.values()) {
            if (creditCardType.equals(type)) {
                return creditCardType;
            }
        }
        return null;
    }
}
