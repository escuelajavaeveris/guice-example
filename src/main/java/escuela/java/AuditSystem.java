package escuela.java;

import com.google.inject.ImplementedBy;

/**
 * Created by hgranjal on 19/06/2018.
 */
@ImplementedBy(AuditSystemES.class)
public interface AuditSystem {
    void audit(String data);
}
